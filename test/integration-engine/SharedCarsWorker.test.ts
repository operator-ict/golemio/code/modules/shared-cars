import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { SharedCarsWorker } from "#ie/SharedCarsWorker";

describe("SharedCarsWorker", () => {
    let worker: SharedCarsWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: { now: new Date("2021-09-20T16:00:00.000Z") } });
        worker = new SharedCarsWorker();
        sandbox.stub(worker["ceskyCarsharingDataSource"], "getAll");
        sandbox.stub(worker["hoppyGoDataSource"], "getAll");
        sandbox.stub(worker["ceskyCarsharingTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["hoppyGoTransformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["model"], "save");
        sandbox.stub(worker["model"], "delete");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        sandbox.assert.calledOnce(worker["ceskyCarsharingDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["ceskyCarsharingTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["hoppyGoDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["hoppyGoTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["ceskyCarsharingDataSource"].getAll as SinonSpy,
            worker["ceskyCarsharingTransformation"].transform as SinonSpy,
            worker["hoppyGoDataSource"].getAll as SinonSpy,
            worker["hoppyGoTransformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy
        );
    });

    it("should call the correct methods by deleteOldSharedBikes method", async () => {
        await worker.deleteOldSharedCars();
        sandbox.assert.calledOnce(worker["model"].delete as SinonSpy);
        sandbox.assert.calledWith(worker["model"].delete as SinonSpy, { "properties.updated_at": { $lt: 1632153480000 } });
    });
});
