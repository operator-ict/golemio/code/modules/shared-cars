import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { SharedCars } from "#sch/index";
import { SharedCarsWorker } from "#ie/SharedCarsWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: SharedCars.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + SharedCars.name.toLowerCase(),
        queues: [
            {
                name: "refreshDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 1 * 60 * 1000, // 1 minute
                },
                worker: SharedCarsWorker,
                workerMethod: "refreshDataInDB",
            },
            {
                name: "deleteOldSharedCars",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: SharedCarsWorker,
                workerMethod: "deleteOldSharedCars",
            },
        ],
    },
];

export { queueDefinitions };
