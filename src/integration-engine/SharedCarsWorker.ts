import cheapruler from "cheap-ruler";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { Validator } from "@golemio/core/dist/shared/golemio-validator";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { MongoModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { SharedCars } from "#sch/index";
import { CeskyCarsharingTransformation, HoppyGoTransformation } from "./";

const ruler = cheapruler(50);

export class SharedCarsWorker extends BaseWorker {
    private ceskyCarsharingDataSource: DataSource;
    private hoppyGoDataSource: DataSource;
    private ceskyCarsharingTransformation: CeskyCarsharingTransformation;
    private hoppyGoTransformation: HoppyGoTransformation;
    private model: MongoModel;

    constructor() {
        super();
        this.ceskyCarsharingDataSource = new DataSource(
            SharedCars.ceskyCarsharing.name + "DataSource",
            new HTTPProtocolStrategy({
                body: JSON.stringify(config.datasources.CeskyCarsharingSharedCarsEndpointCredentials),
                headers: {
                    "Content-Type": "application/json",
                },
                method: "POST",
                url: config.datasources.CeskyCarsharingSharedCars,
            }),
            new JSONDataTypeStrategy({ resultsPath: "cars" }),
            new Validator(
                SharedCars.ceskyCarsharing.name + "DataSource",
                SharedCars.ceskyCarsharing.datasourceMongooseSchemaObject
            )
        );
        const hoppyGoDataType = new JSONDataTypeStrategy({ resultsPath: "" });
        hoppyGoDataType.setFilter((item) => item.localization && item.localization !== "" && item.localization !== ",");
        this.hoppyGoDataSource = new DataSource(
            SharedCars.hoppyGo.name + "DataSource",
            new HTTPProtocolStrategy({
                headers: {
                    "x-app-token": config.datasources.HoppyGoSharedCarsToken,
                },
                method: "GET",
                url: config.datasources.HoppyGoSharedCars,
            }),
            hoppyGoDataType,
            new Validator(SharedCars.hoppyGo.name + "DataSource", SharedCars.hoppyGo.datasourceMongooseSchemaObject)
        );

        this.model = new MongoModel(
            SharedCars.name + "Model",
            {
                identifierPath: "properties.id",
                mongoCollectionName: SharedCars.mongoCollectionName,
                outputMongooseSchemaObject: SharedCars.outputMongooseSchemaObject,
                resultsPath: "properties",
                savingType: "insertOrUpdate",
                searchPath: (id, multiple) => (multiple ? { "properties.id": { $in: id } } : { "properties.id": id }),
                updateValues: (a, b) => {
                    a.geometry.coordinates = b.geometry.coordinates;
                    a.properties.availability = b.properties.availability;
                    a.properties.company = b.properties.company;
                    a.properties.fuel = b.properties.fuel;
                    a.properties.name = b.properties.name;
                    a.properties.res_url = b.properties.res_url;
                    a.properties.updated_at = b.properties.updated_at;
                    return a;
                },
            },
            new Validator(SharedCars.name + "ModelValidator", SharedCars.outputMongooseSchemaObject)
        );
        this.ceskyCarsharingTransformation = new CeskyCarsharingTransformation();
        this.hoppyGoTransformation = new HoppyGoTransformation();
    }

    public refreshDataInDB = async (msg: any): Promise<void> => {
        let ceskyCarsharing = [];
        let hoppyGo = [];

        try {
            ceskyCarsharing = await this.ceskyCarsharingTransformation.transform(await this.ceskyCarsharingDataSource.getAll());
        } catch (err) {
            throw new CustomError("Error while getting data - ceskyCarsharing.", true, this.constructor.name, 2001, err);
        }

        try {
            hoppyGo = await this.hoppyGoTransformation.transform(await this.hoppyGoDataSource.getAll());
        } catch (err) {
            throw new CustomError("Error while getting data - hoppyGo.", true, this.constructor.name, 2001, err);
        }

        try {
            const concatenatedData = [...ceskyCarsharing, ...hoppyGo];

            // filter the objects 18 km far from the center of Prague
            // or the objects 7 km far from the center of Liberec
            const filteredData = concatenatedData.filter((item) => {
                // distance from center of Prague
                return (
                    this.isInDistanceFrom(item.geometry.coordinates, [14.463401734828949, 50.06081863605803], 18) ||
                    // or distance from center of Liberec
                    this.isInDistanceFrom(item.geometry.coordinates, [15.057755, 50.771333], 7)
                );
            });

            await this.model.save(filteredData);
        } catch (err) {
            throw new CustomError("Error while saving data.", true, this.constructor.name, 4001, err);
        }
    };

    public deleteOldSharedCars = async (): Promise<void> => {
        const now = new Date();
        const ttl = new Date();
        ttl.setMinutes(now.getMinutes() - 2);

        try {
            await this.model.delete({
                "properties.updated_at": { $lt: ttl.getTime() },
            });
        } catch (err) {
            throw new CustomError("Error while purging old data.", true, this.constructor.name, 5002, err);
        }
    };

    private isInDistanceFrom = (itemCoords: cheapruler.Point, targetCoords: cheapruler.Point, distance: number): boolean => {
        const realDist = ruler.distance(targetCoords, itemCoords);
        return realDist < distance;
    };
}
