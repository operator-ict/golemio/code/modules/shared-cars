import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject

const ceskyCarsharingDatasourceMSO: SchemaDefinition = {
    car_name: { type: String, required: true },
    company_email: { type: String },
    company_name: { type: String },
    company_phone: { type: String },
    company_web: { type: String, required: true },
    fuel: { type: Number, required: true },
    fuel_type: { type: String },
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    res_url: { type: String, required: true },
    rz: { type: String, required: true },
    status: { type: Number },
};

const hoppyGoDatasourceMSO: SchemaDefinition = {
    hash_code: { type: String },
    id: { type: String, required: true },
    localization: { type: String, required: true },
    manufacturer_name: { type: String, required: true },
    model_name: { type: String, required: true },
    photo_hash_code: { type: String },
    photo_mime: { type: String },
    photo_size: { type: String },
    price_per_day: { type: String },
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        availability: {
            description: { type: String },
            id: { type: Number, required: true },
        },
        company: {
            email: { type: String },
            name: { type: String, required: true },
            phone: { type: String },
            web: { type: String, required: true },
        },
        fuel: {
            description: { type: String },
            id: { type: Number, required: true },
        },
        id: { type: String, required: true },
        name: { type: String, required: true },
        res_url: { type: String, required: true },
        updated_at: { type: Number, required: true },
    },
    type: { type: String, required: true },
};

const forExport = {
    ceskyCarsharing: {
        datasourceMongooseSchemaObject: ceskyCarsharingDatasourceMSO,
        name: "CeskyCarsharingSharedCars",
    },
    hoppyGo: {
        datasourceMongooseSchemaObject: hoppyGoDatasourceMSO,
        name: "HoppyGoSharedCars",
    },
    mongoCollectionName: "sharedcars",
    name: "SharedCars",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as SharedCars };
