# Implementační dokumentace modulu _shared-cars (Sdílená auta)_

## Záměr

Modul slouží k ukládání informací o sdílených autech v Praze a Liberci.

**Dostupní poskytovatelé**:

-   Český carsharing
-   HoppyGo

## Vstupní data

### Data nám jsou posílána

Nejsou.

### Data aktivně stahujeme

Data stahujeme v pravidelných intervalech

#### Datový zdroj _Český carsharing_

-   zdroj dat
    -   [Český carsharing zdroj](https://api.ceskycarsharing.cz/iis_cs_api/IIS_API.svc/getAllCarsIct)
    -   [IE config - CeskyCarsharingSharedCars, CeskyCarsharingSharedCarsEndpointCredentials](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/blob/development/config/datasources.default.json)
    -   parametry v security
-   formát dat
    -   HTTP POST
    -   json
    -   [validační schéma - ceskyCarsharingDatasourceMSO](../src/schema-definitions/index.ts)

**Autorizace**

Autorizovat se jde nastavením _guid_ a _password_ v těle requestu

```json
{
    "guid": "...",
    "password": "..."
}
```

**Ukázka dat**

```json
{
    "message": "OK",
    "status": 1,
    "cars": [
        {
            "car_name": "Fabia III kombi",
            "company_email": "test@test.cz",
            "company_name": "TEST",
            "company_phone": "+420 222 222 222",
            "company_web": "https://www.test.cz/",
            "fuel": 3,
            "fuel_type": "benzín + LPG",
            "latitude": 50.0735,
            "longitude": 14.4081,
            "res_url": "https://www.test.cz/res/TEST01",
            "rz": "TEST01",
            "status": 1
        }
    ]
}
```

-   frekvence stahování
    -   [dev](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_development/golemio_golemio-values.yaml)
        -   `0 26 7,13,19,1 * * *`, tzn. v 07:26, 13:26, 19:26 a 01:26
    -   [prod](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_golemio-values.yaml)
        -   `0 */2 * * * *`, tzn. každé 2 minuty
-   název rabbitmq fronty
    -   `dataplatform.sharedcars.refreshDataInDB`
    -   bez parametrů
    -   sdílená fronta s ostatními datovými zdroji

#### Datový zdroj _HoppyGo_

-   zdroj dat
    -   HoppyGo zdroj - neveřejné url
    -   [IE config - HoppyGoSharedCars, HoppyGoSharedCarsToken](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/blob/development/config/datasources.default.json)
    -   parametry v security
-   formát dat
    -   HTTP GET
    -   json
    -   [validační schéma - hoppyGoDatasourceMSO](../src/schema-definitions/index.ts)

**Autorizace**

Autorizovat se jde nastavením headeru _x-app-token_ (viz security)

**Ukázka dat**

```json
[
    {
        "id": 42,
        "hash_code": "https://test.cz/test",
        "url": "https://test.cz/test",
        "vehicle_hash_code": "test",
        "manufacturer_name": "Volvo",
        "model_name": "V40",
        "fuel_description": "petrol",
        "price_per_day": 4200,
        "localization": "50.0735,14.4081",
        "photo_hash_code": "https://test.cz/test.jpeg",
        "photo_size": 2561638,
        "photo_mime": "image/jpeg",
        "average_rating": 0
    }
]
```

-   frekvence stahování
    -   sdílená fronta s ostatními datovými zdroji

## Zpracování dat / transformace

### Worker _SharedCarsWorker_

[Odkaz](../src/integration-engine/SharedCarsWorker.ts). Obsahuje metody pro stahování ze všech datových zdrojů, transformaci a retenci dat.

#### Metoda _refreshDataInDB_

-   vstupní rabbitmq fronta
    -   `dataplatform.sharedcars.refreshDataInDB`
    -   bez parametrů
-   datové zdroje
    -   všechny (viz výše)
-   transformace
    -   [`CeskyCarsharingTransformation`](../src/integration-engine/CeskyCarsharingTransformation.ts)
    -   [`HoppyGoTransformation`](../src/integration-engine/HoppyGoTransformation.ts)
-   data modely
    -   definovaný ve workeru
    -   koresponduje s kolekcí `sharedcars`
    -   (viz níže)

#### Metoda _deleteOldSharedCars_

-   vstupní rabbitmq fronta
    -   `dataplatform.sharedcars.deleteOldSharedCars`
    -   promaže auta starší než 2 minuty
    -   voláno vždy 30 vteřin po stažení aut (viz metoda _refreshDataInDB_)

## Uložení dat

Data se ukládají do Monga.

### Obecné

-   typ databáze
    -   MongoDB
-   datábázové schéma
    -   [diagram](./mongodb_diagram.md)
    -   MongoDB
        -   kolekce `sharedcars`
        -   struktura [outputMSO](../src/schema-definitions/index.ts)
        -   migrační skripty jsou součástí sdíleních ve schema-definitions [složka](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/tree/development/db/mongodb-migrations)
        -   example data [složka](../db/example/mongo_data) (slouží i pro testy)
-   retence dat
    -   dynamická data se promazávají viz _SharedCarsWorker_ výše

## Output API

Není k dispozici.
