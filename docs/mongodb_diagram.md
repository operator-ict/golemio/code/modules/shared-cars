```mermaid
%% https://mermaid-js.github.io/mermaid/#/classDiagram
classDiagram
    direction LR

    Car --o Geometry
    Car --o Properties
    Properties --o Availability
    Properties --o Company
    Properties --o Fuel

    class Car {
        type string
        geometry Geometry
        properties Properties
    }

    class Geometry{
        coordinates float[]
        type string
    }

    class Properties {
        id string
        name string
        res_url string
        updated_at: int
        availability Availability
        company Company
        fuel Fuel
    }

    class Availability {
        id int
        description string
    }

    class Company {
        email string
        name string
        phone string
        web string
    }

    class Fuel {
        id int
        description string
    }
```
